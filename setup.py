import os

from setuptools import setup, find_packages


setup(name='mez',
    version='0.0.1',
    description='A Mezzanine based CMS project',
    packages=find_packages('src'),
    package_dir={'': 'src'},
    include_package_data=True,
    zip_safe=False,
    author='James Pic',
    author_email='jamespic@gmail.com',
    url='https://gitlab.com/yourlabs/mez',
    install_requires=[
        'beautifulsoup4~=4.5',
        'Cartridge~=0.12.0',
        'Django~=1.10',
        'django-colorful~=1.2',
        'django-contrib-comments~=1.7',
        'django-compressor~=2.1',
        'django-redis~=4.5',
        'easy-thumbnails~=2.3',
        'filebrowser-safe~=0.4',
        'html5lib==0.9999999',
        'Markdown~=2.6',
        'Mezzanine~=4.2',
        'django-modeltranslation==0.12',
        'oauthlib~=2.0',
        'pytz',
        'raven',
        'six',
        'tzlocal<2.0',
        'webcolors==1.5',
    ],
    extras_require={
        # Full version hardcode for testing dependencies so that
        # tests don't break on master without any obvious reason.
        'testing': [
            'codecov>=2,<3',
            'flake8>=2,<3',
            'pep8>=1,<2',
            'pytest>=2,<3',
            'pytest-django>=2,<3',
            'pytest-cov>=2,<3',
            'mock==2.0.0',
            'tox>=2.3,<3',
        ]
    },
    entry_points={
        'console_scripts': [
            'mez = mez.manage:main',
        ]
    }
)
