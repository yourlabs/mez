from django.db import models

from mezzanine.pages.models import Page


class HtmlPage(Page):
    html = models.TextField()
