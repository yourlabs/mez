from modeltranslation.translator import translator, TranslationOptions

from .models import HtmlPage


class TranslatedHtmlPage(TranslationOptions):
    fields = ('html',)


translator.register(HtmlPage, TranslatedHtmlPage)
