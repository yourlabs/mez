from django.contrib import admin
from mezzanine.pages.models import RichTextPage
from mezzanine.forms.models import Form
from mezzanine.galleries.models import Gallery
from mezzanine.blog.models import BlogPost
from cartridge.shop.models import Category, Product

from mezzanine.pages.admin import PageAdmin

from .models import HtmlPage


admin.site.register(HtmlPage, PageAdmin)

for m in (BlogPost, Form, Gallery, RichTextPage, HtmlPage, Category, Product):
    a = admin.site._registry[m]
    a.fieldsets[0][1]['fields'].insert(
        -2,
        'background_image',
    )
    a.fieldsets[0][1]['fields'].insert(
        -2,
        (
            'css',
        ),
    )

