from django.core.cache import cache
from django.db import models


class FontManager(models.Manager):
    def get_font(self, pk):
        """Cached font fetch, updated if font is not cached."""
        font_cache = cache.get('font_cache', {})

        if not font_cache or pk not in font_cache:
            for font in Font.objects.all():
                font_cache[font.pk] = {
                    'font_url': font.font_file.url if font.font_file else '',
                    'stylesheet_url': font.stylesheet_url,
                    'name': font.name,
                }
            cache.set('font_cache', font_cache)

        return font_cache.get(int(pk))


class Font(models.Model):
    font_file = models.FileField(
        upload_to='fonts',
        blank=True
    )

    stylesheet_url = models.URLField(
        blank=True,
    )

    name = models.CharField(
        max_length=50,
        help_text='Font-family name, **MUST** match the name in the file'
    )

    objects = FontManager()

    def __unicode__(self):
        return self.name
