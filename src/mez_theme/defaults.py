from mezzanine.conf import register_setting


class FontIterator(object):
    def __iter__(self):
        from .models import Font
        for font in Font.objects.all():
            yield (font.pk, font.name)

    def __len__(self):
        from .models import Font
        return Font.objects.all().count()

register_setting(
    name='FONT_LOGO',
    label='Font to use on the logo',
    editable=True,
    default='',
    choices=FontIterator()
)

register_setting(
    name='FONT_HEADER',
    label='Font to use on headers',
    editable=True,
    default='',
    choices=FontIterator()
)

register_setting(
    name='FONT_PARAGRAPH',
    label='Font to use for paragraphs',
    editable=True,
    default='',
    choices=FontIterator()
)

register_setting(
    name='FONT_CODE',
    label='Font to use for code',
    editable=True,
    default='',
    choices=FontIterator()
)

register_setting(
    name='SHOW_SEARCH',
    label='Search feature',
    description='Display the search box in the navigation header',
    editable=True,
    default=False,
)

register_setting(
    name='CONTACT_NAME',
    label='Contact name',
    description='Name of the person or org to contact, shown in footer',
    editable=True,
    default='',
)

register_setting(
    name='CONTACT_LOCATION',
    label='Contact location',
    description='Location of the contact, shown in footer',
    editable=True,
    default='',
)

register_setting(
    name='CONTACT_NUMBER',
    label='Contact phone number',
    description='Phone number of the contact, displayed in footer',
    editable=True,
    default='',
)

# Append the Cartridge settings used in templates to the list of settings
# accessible in templates.
register_setting(
    name='TEMPLATE_ACCESSIBLE_SETTINGS',
    description='Sequence of setting names available within templates.',
    editable=False,
    default=(
        'FONT_CODE',
        'FONT_HEADER',
        'FONT_PARAGRAPH',
        'CONTACT_LOCATION',
        'CONTACT_NAME',
        'CONTACT_NUMBER',
        'SHOW_SEARCH',
    ),
    append=True,
)
