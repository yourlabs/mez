import string

from django import template
from django.core.cache import cache
from django.utils.safestring import mark_safe

from mezzanine.conf import settings

from ..models import Font

register = template.Library()


def html_font_face_declaration(font):
    font_formats = {
        'ttf': 'truetype',
        'woff': 'woff',
    }

    if font['font_url']:
        for ext, name in font_formats.items():
            if font['font_url'].endswith(ext):
                font['format'] = name
                return '''
                    <style type="text/css">
                    @font-face {{
                        font-family: "{name}";
                        src: url("{font_url}") format("{format}");
                    }}
                    </style>
                '''.format(**font)

    elif font['stylesheet_url']:
        return '''
        <link href="{stylesheet_url}" rel="stylesheet">
        '''.format(**font)


@register.filter
def css_font_declarations(request):
    _map = (
        (
            settings.FONT_LOGO,
            'a.navbar-brand'
        ),
        (
            settings.FONT_CODE,
            'body pre, body code'
        ),
        (
            settings.FONT_HEADER,
            ','.join(
                ['body h%s' % i for i in range(1, 4)]
                + ['.navbar-brand', '.navbar-nav li a', '.font-header']
            ),
        ),
        (
            settings.FONT_PARAGRAPH,
            ','.join(
                ['body h%s' % i for i in range(4, 7)]
                + ['body %s' % i for i in ('p', 'blockquote', 'a')]
            ),
        ),
    )

    html = []
    for pk, target in _map:
        if not pk:
            continue

        font = Font.objects.get_font(pk)
        html.append(html_font_face_declaration(font))
        html.append('<style type="text/css">')
        html.append('%s { font-family: "%s"; }' % (target, font['name']))
        html.append('</style>')

    return mark_safe('\n'.join(html))


@register.filter
def invert_color(color):
    table = string.maketrans(
        '0123456789abcdef',
        'fedcba9876543210')

    color = str(color.replace('#', ''))
    return '#%s' % color.lower().translate(table).upper()
