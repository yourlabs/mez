# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2016-12-12 17:36
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pages', '0009_add_page_extra_body_class'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='page',
            name='align_items',
        ),
        migrations.RemoveField(
            model_name='page',
            name='background_color',
        ),
        migrations.RemoveField(
            model_name='page',
            name='background_color_transparency',
        ),
        migrations.RemoveField(
            model_name='page',
            name='background_position',
        ),
        migrations.RemoveField(
            model_name='page',
            name='background_size',
        ),
        migrations.RemoveField(
            model_name='page',
            name='content_format',
        ),
        migrations.RemoveField(
            model_name='page',
            name='extra_body_class',
        ),
        migrations.RemoveField(
            model_name='page',
            name='extra_html',
        ),
        migrations.RemoveField(
            model_name='page',
            name='full_screen',
        ),
        migrations.AddField(
            model_name='page',
            name='css',
            field=models.TextField(blank=True, default='', verbose_name='CSS'),
        ),
    ]
