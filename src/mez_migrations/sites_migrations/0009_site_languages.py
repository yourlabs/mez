# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-01-25 14:44
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sites', '0008_simplify_leaves_css_only'),
    ]

    operations = [
        migrations.AddField(
            model_name='site',
            name='languages',
            field=models.CharField(blank=True, default='en,fr', max_length=200, verbose_name='Languages'),
        ),
    ]
