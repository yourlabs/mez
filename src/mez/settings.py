# -*- coding: utf8 -*-
from __future__ import absolute_import, unicode_literals
import os
import sys

from django import VERSION as DJANGO_VERSION
from django.utils.translation import ugettext_lazy as _

DEBUG = bool(os.getenv('DEBUG'))

RICHTEXT_FILTER_LEVEL = 3
#
#PAGEDOWN_SERVER_SIDE_PREVIEW = True
#PAGEDOWN_MARKDOWN_EXTENSIONS = ('extra','codehilite','toc')

THUMBNAIL_ALIASES = {
    '': {
    },
}

for i in (1, 1.5, 2, 3, 4, 5, 6, 7, 8, 9, 10):
    size = int(i * 150)

    THUMBNAIL_ALIASES['']['%sw' % size] = {
        'size': (size, 0),
        'crop': 'scale',
    }

DATABASES = {
    'default': {
        'ENGINE': os.getenv(
            'DB_ENGINE',
            'django.db.backends.postgresql_psycopg2'
        ),
        'NAME': os.getenv('DB_NAME', os.getenv('USER')),
        'USER': os.getenv('DB_USER', os.getenv('USER')),
    }
}

LANGUAGES = (
    ('en', _('English')),
    ('be_BY', _('Belarus')),
    ('cy_GB', _('Welsh')),
    ('de_DE', _('German')),
    ('en_US', _('American')),
    ('es_ES', _('Spanish')),
    ('fi_FI', _('Finnish')),
    ('fr_FR', _('French')),
    ('hr_HR', _('Hungarian')),
    ('it_IT', _('Italian')),
    ('lt_LT', _('Lituanian')),
    ('lv_LV', _('Latvian')),
    ('mt_MT', _('Maltese')),
    ('nl_NL', _('Dutch')),
    ('pl_PL', _('Polish')),
    ('pt_BR', _('Portuguese')),
    ('ro_RO', _('Romanian')),
    ('sk_SK', _('Slovak')),
)

SECRET_KEY = os.getenv('SECRET_KEY') or 'notsecret'
NEVERCACHE_KEY = os.getenv('NEVERCACHE_KEY', 'notakey')

CACHES = {
    'default': {
        'BACKEND': 'django_redis.cache.RedisCache',
        'LOCATION': 'redis://127.0.0.1:6379/1',
        'OPTIONS': {
            'CLIENT_CLASS': 'django_redis.client.DefaultClient',
        }
    }
}

SESSION_ENGINE = 'django.contrib.sessions.backends.cache'
SESSION_CACHE_ALIAS = 'default'

EXTRA_MODEL_FIELDS = (
    (
        'mezzanine.pages.models.Page.background_image',
        'mezzanine.core.fields.FileField',
        ('Image',),
        dict(null=True, blank=True, upload_to='', format='image'),
    ),
    (
        'mezzanine.pages.models.Page.css',
        'TextField',
        ('CSS',),
        dict(
            default='',
            blank=True,
        ),
    ),
    (
        'cartridge.shop.models.Product.background_image',
        'mezzanine.core.fields.FileField',
        ('Image',),
        dict(null=True, blank=True, upload_to='', format='image'),
    ),
    (
        'cartridge.shop.models.Product.css',
        'TextField',
        ('CSS',),
        dict(
            default='',
            blank=True,
        ),
    ),
    (
        'django.contrib.sites.models.Site.css',
        'TextField',
        ('CSS',),
        dict(
            default='',
            blank=True,
        ),
    ),
    (
        'django.contrib.sites.models.Site.logo',
        'mezzanine.core.fields.FileField',
        ('Image',),
        dict(null=True, blank=True, upload_to='', format='image'),
    ),
    (
        'django.contrib.sites.models.Site.languages',
        'ManyToManyField',
        ('languages.Language', 'Languages',),
        dict(
            blank=True,
        ),
    ),
)


MIGRATION_MODULES = {
    'pages': 'mez_migrations.pages_migrations',
    'blog': 'mez_migrations.blog_migrations',
    'conf': 'mez_migrations.conf_migrations',
    'galleries': 'mez_migrations.galleries_migrations',
    'forms': 'mez_migrations.forms_migrations',
    'shop': 'mez_migrations.shop_migrations',
    'sites': 'mez_migrations.sites_migrations',
}

FILEBROWSER_EXCLUDE = (
    '_(.html|.py|.js|.css|.jpg|.jpeg|.gif|.png|.tif|.tiff|.svg|.mp3|.wav|.aiff|.midi|.m4p|.mov|.wmv|.mpeg|.mpg|.avi|.rm|.mp4||.pdf|.doc|.rtf|.txt|.xls|.csv|.docx)_.*_q\\d{1,3}\\.(.html|.py|.js|.css|.jpg|.jpeg|.gif|.png|.tif|.tiff|.svg|.mp3|.wav|.aiff|.midi|.m4p|.mov|.wmv|.mpeg|.mpg|.avi|.rm|.mp4||.pdf|.doc|.rtf|.txt|.xls|.csv|.docx)|.jpg.\d+x\d+_q\d+_.*.jpg|.png.\d+x\d+_q\d+_.*.png',
)
#.jpg.\d+x\d+_q\d+_.*.jpg

######################
# CARTRIDGE SETTINGS #
######################

# The following settings are already defined in cartridge.shop.defaults
# with default values, but are common enough to be put here, commented
# out, for conveniently overriding. Please consult the settings
# documentation for a full list of settings Cartridge implements:
# http://cartridge.jupo.org/configuration.html#default-settings

# Sequence of available credit card types for payment.
# SHOP_CARD_TYPES = ('Mastercard', 'Visa', 'Diners', 'Amex')

# Setting to turn on featured images for shop categories. Defaults to False.
# SHOP_CATEGORY_USE_FEATURED_IMAGE = True

# Set an alternative OrderForm class for the checkout process.
# SHOP_CHECKOUT_FORM_CLASS = 'cartridge.shop.forms.OrderForm'

# If True, the checkout process is split into separate
# billing/shipping and payment steps.
# SHOP_CHECKOUT_STEPS_SPLIT = True

# If True, the checkout process has a final confirmation step before
# completion.
# SHOP_CHECKOUT_STEPS_CONFIRMATION = True

# Controls the formatting of monetary values accord to the locale
# module in the python standard library. If an empty string is
# used, will fall back to the system's locale.
SHOP_CURRENCY_LOCALE = 'fr_FR.UTF-8'

# Dotted package path and name of the function that
# is called on submit of the billing/shipping checkout step. This
# is where shipping calculation can be performed and set using the
# function ``cartridge.shop.utils.set_shipping``.
# SHOP_HANDLER_BILLING_SHIPPING = \
#                       'cartridge.shop.checkout.default_billship_handler'

# Dotted package path and name of the function that
# is called once an order is successful and all of the order
# object's data has been created. This is where any custom order
# processing should be implemented.
# SHOP_HANDLER_ORDER = 'cartridge.shop.checkout.default_order_handler'

# Dotted package path and name of the function that
# is called on submit of the payment checkout step. This is where
# integration with a payment gateway should be implemented.
# SHOP_HANDLER_PAYMENT = 'cartridge.shop.checkout.default_payment_handler'

# Sequence of value/name pairs for order statuses.
# SHOP_ORDER_STATUS_CHOICES = (
#     (1, 'Unprocessed'),
#     (2, 'Processed'),
# )

# Sequence of value/name pairs for types of product options,
# eg Size, Colour. NOTE: Increasing the number of these will
# require database migrations!
SHOP_OPTION_TYPE_CHOICES = (
    (1, 'Size'),
    (2, 'Colour'),
    (3, 'Cognac Label'),
)

# Sequence of indexes from the SHOP_OPTION_TYPE_CHOICES setting that
# control how the options should be ordered in the admin,
# eg for 'Colour' then 'Size' given the above:
# SHOP_OPTION_ADMIN_ORDER = (2, 1)


######################
# MEZZANINE SETTINGS #
######################

# The following settings are already defined with default values in
# the ``defaults.py`` module within each of Mezzanine's apps, but are
# common enough to be put here, commented out, for conveniently
# overriding. Please consult the settings documentation for a full list
# of settings Mezzanine implements:
# http://mezzanine.jupo.org/docs/configuration.html#default-settings

# Controls the ordering and grouping of the admin menu.
#
# ADMIN_MENU_ORDER = (
#     ('Content', ('pages.Page', 'blog.BlogPost',
#        'generic.ThreadedComment', (_('Media Library'), 'fb_browse'),)),
#     (_('Shop'), ('shop.Product', 'shop.ProductOption', 'shop.DiscountCode',
#        'shop.Sale', 'shop.Order')),
#     ('Site', ('sites.Site', 'redirects.Redirect', 'conf.Setting')),
#     ('Users', ('auth.User', 'auth.Group',)),
# )

# A three item sequence, each containing a sequence of template tags
# used to render the admin dashboard.
#
# DASHBOARD_TAGS = (
#     ('blog_tags.quick_blog', 'mezzanine_tags.app_list'),
#     ('comment_tags.recent_comments',),
#     ('mezzanine_tags.recent_actions',),
# )

# A sequence of templates used by the ``page_menu`` template tag. Each
# item in the sequence is a three item sequence, containing a unique ID
# for the template, a label for the template, and the template path.
# These templates are then available for selection when editing which
# menus a page should appear in. Note that if a menu template is used
# that doesn't appear in this setting, all pages will appear in it.

# PAGE_MENU_TEMPLATES = (
#     (1, _('Top navigation bar'), 'pages/menus/dropdown.html'),
#     (2, _('Left-hand tree'), 'pages/menus/tree.html'),
#     (3, _('Footer'), 'pages/menus/footer.html'),
# )

# A sequence of fields that will be injected into Mezzanine's (or any
# library's) models. Each item in the sequence is a four item sequence.
# The first two items are the dotted path to the model and its field
# name to be added, and the dotted path to the field class to use for
# the field. The third and fourth items are a sequence of positional
# args and a dictionary of keyword args, to use when creating the
# field instance. When specifying the field class, the path
# ``django.models.db.`` can be omitted for regular Django model fields.
#
# EXTRA_MODEL_FIELDS = (
#     (
#         # Dotted path to field.
#         'mezzanine.blog.models.BlogPost.image',
#         # Dotted path to field class.
#         'somelib.fields.ImageField',
#         # Positional args for field class.
#         (_('Image'),),
#         # Keyword args for field class.
#         {'blank': True, 'upload_to': 'blog'},
#     ),
#     # Example of adding a field to *all* of Mezzanine's content types:
#     (
#         'mezzanine.pages.models.Page.another_field',
#         'IntegerField', # 'django.db.models.' is implied if path is omitted.
#         (_('Another name'),),
#         {'blank': True, 'default': 1},
#     ),
# )

# Setting to turn on featured images for blog posts. Defaults to False.
#
# BLOG_USE_FEATURED_IMAGE = True


# If True, the django-modeltranslation will be added to the
# INSTALLED_APPS setting.
USE_MODELTRANSLATION = not 'makemigrations' in sys.argv and not 'migrate' in sys.argv

COMPRESS_PRECOMPILERS = (
    ('text/x-scss', 'django_libsass.SassCompiler'),
)


########################
# MAIN DJANGO SETTINGS #
########################

# Hosts/domain names that are valid for this site; required if DEBUG is False
# See https://docs.djangoproject.com/en/dev/ref/settings/#allowed-hosts
ALLOWED_HOSTS = []
if 'ALLOWED_HOSTS' in os.environ:
    ALLOWED_HOSTS = os.getenv('ALLOWED_HOSTS').split(',')

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# On Unix systems, a value of None will cause Django to use the same
# timezone as the operating system.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'UTC'

# If you set this to True, Django will use timezone-aware datetimes.
USE_TZ = True

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en'

# Whether a user's session cookie expires when the Web browser is closed.
SESSION_EXPIRE_AT_BROWSER_CLOSE = True

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

AUTHENTICATION_BACKENDS = ('mezzanine.core.auth_backends.MezzanineBackend',)

# The numeric mode to set newly-uploaded files to. The value should be
# a mode you'd pass directly to os.chmod.
FILE_UPLOAD_PERMISSIONS = 0o644

#########
# PATHS #
#########

# Full filesystem path to the project.
PROJECT_APP_PATH = os.path.dirname(os.path.abspath(__file__))
PROJECT_APP = os.path.basename(PROJECT_APP_PATH)
PROJECT_ROOT = BASE_DIR = os.path.dirname(PROJECT_APP_PATH)

# Every cache key will get prefixed with this value - here we set it to
# the name of the directory the project is in to try and use something
# project specific.
CACHE_MIDDLEWARE_KEY_PREFIX = PROJECT_APP

# URL prefix for static files.
# Example: 'http://media.lawrence.com/static/'
STATIC_URL = os.getenv('STATIC_URL', '/static/')

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' 'static/' subdirectories and in STATICFILES_DIRS.
# Example: '/home/media/media.lawrence.com/static/'
STATIC_ROOT = os.getenv(
    'STATIC_ROOT',
    os.path.join(PROJECT_ROOT, STATIC_URL.strip('/'))
)

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: 'http://media.lawrence.com/media/', 'http://example.com/media/'
MEDIA_URL = os.getenv('MEDIA_URL', STATIC_URL + 'media/')

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: '/home/media/media.lawrence.com/media/'
MEDIA_ROOT = os.getenv(
    'MEDIA_ROOT',
    os.path.join(PROJECT_ROOT, *MEDIA_URL.strip('/').split('/'))
)

# Package/module name to import the root urlpatterns from for the project.
ROOT_URLCONF = '%s.urls' % PROJECT_APP

MAX_UPLOAD_SIZE = 20971520
FILEBROWSER_MAX_UPLOAD_SIZE = MAX_UPLOAD_SIZE

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            os.path.join(os.path.dirname(__file__), 'templates')
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'django.template.context_processors.debug',
                'languages.context_processors.i18n',
                'django.template.context_processors.static',
                'django.template.context_processors.media',
                'django.template.context_processors.request',
                'django.template.context_processors.tz',
                'mezzanine.conf.context_processors.settings',
                'mezzanine.pages.context_processors.page',
            ],
            'builtins': [
                'mezzanine.template.loader_tags',
            ],
        },
    },
]

if DJANGO_VERSION < (1, 9):
    del TEMPLATES[0]['OPTIONS']['builtins']

RAVEN_CONFIG = {
    'dsn': os.getenv('RAVEN_DSN', False)
}


if os.getenv('LOG'):
    LOGGING = {
        'version': 1,
        'disable_existing_loggers': False,
        'handlers': {
            'file.error': {
                'level': 'ERROR',
                'class': 'logging.FileHandler',
                'filename': os.path.join(
                    os.getenv('LOG'),
                    'django.error.log',
                )
            },
            'file.info': {
                'level': 'INFO',
                'class': 'logging.FileHandler',
                'filename': os.path.join(
                    os.getenv('LOG'),
                    'django.info.log',
                )
            },
            'file.debug': {
                'level': 'DEBUG',
                'class': 'logging.FileHandler',
                'filename': os.path.join(
                    os.getenv('LOG'),
                    'django.debug.log',
                )
            },
        },
        'loggers': {
            'django': {
                'handlers': ['file.error', 'file.info', 'file.debug',],
                'level': 'DEBUG',
                'propagate': True,
            },
        },
    }
#
################
# APPLICATIONS #
################

INSTALLED_APPS = (
    # My theme
    'mez_theme',

    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.redirects',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.sitemaps',
    'django.contrib.staticfiles',
    'mezzanine.boot',
    'mezzanine.conf',
    'mezzanine.core',
    'mezzanine.generic',
    'mezzanine.pages',
    'cartridge.shop',
    'mezzanine.blog',
    'mezzanine.forms',
    'mezzanine.galleries',
    'mezzanine.twitter',
    # 'mezzanine.accounts',
    # 'mezzanine.mobile',

    # My external addons
    'raven.contrib.django.raven_compat',
    'easy_thumbnails',

    # My hacks
    'languages',
    'mez_migrations',
    'mez_app',
    'bgset',
)

# List of middleware classes to use. Order is important; in the request phase,
# these middleware classes will be applied in the order given, and in the
# response phase the middleware will be applied in reverse order.
MIDDLEWARE_CLASSES = (
    'mezzanine.core.middleware.UpdateCacheMiddleware',

    'django.contrib.sessions.middleware.SessionMiddleware',
    # Uncomment if using internationalisation or localisation
    # 'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.sites.middleware.CurrentSiteMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',

    'cartridge.shop.middleware.ShopMiddleware',
    'mezzanine.core.request.CurrentRequestMiddleware',
    'mezzanine.core.middleware.RedirectFallbackMiddleware',
    'mezzanine.core.middleware.TemplateForDeviceMiddleware',
    'mezzanine.core.middleware.TemplateForHostMiddleware',
    'mezzanine.core.middleware.AdminLoginInterfaceSelectorMiddleware',
    'mezzanine.core.middleware.SitePermissionMiddleware',
    # Uncomment the following if using any of the SSL settings:
    # 'mezzanine.core.middleware.SSLRedirectMiddleware',
    'mezzanine.pages.middleware.PageMiddleware',
    'mezzanine.core.middleware.FetchFromCacheMiddleware',
)

# Store these package names here as they may change in the future since
# at the moment we are using custom forks of them.
PACKAGE_NAME_FILEBROWSER = 'filebrowser_safe'
PACKAGE_NAME_GRAPPELLI = 'grappelli_safe'

#########################
# OPTIONAL APPLICATIONS #
#########################

# These will be added to ``INSTALLED_APPS``, only if available.
OPTIONAL_APPS = (
    'debug_toolbar',
    'django_extensions',
    'compressor',
    PACKAGE_NAME_FILEBROWSER,
    PACKAGE_NAME_GRAPPELLI,
)

####################
# DYNAMIC SETTINGS #
####################

# set_dynamic_settings() will rewrite globals based on what has been
# defined so far, in order to provide some better defaults where
# applicable. We also allow this settings module to be imported
# without Mezzanine installed, as the case may be when using the
# fabfile, where setting the dynamic settings below isn't strictly
# required.
try:
    from mezzanine.utils.conf import set_dynamic_settings
except ImportError:
    pass
else:
    set_dynamic_settings(globals())
