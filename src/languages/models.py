from django.db import models

from mezzanine.core.fields import FileField


class Language(models.Model):
    code = models.CharField(max_length=2)
    name = models.CharField(max_length=50)
    territory = models.CharField(max_length=2)
    flag = models.FileField(null=True, blank=True)

    class Meta:
        unique_together = ('code', 'territory')

    def __unicode__(self):
        return self.name

    @property
    def python_name(self):
        return '%s_%s' % (self.code, self.territory.upper())

    @property
    def posix(self):
        return '%s.UTF-8' % self.python_name
