from django.utils.translation import ugettext_lazy as _


def i18n(request):
    from django.utils import translation
    return {
        'languages': [
            (i.python_name, _(i.name))
            for i in request.site.languages.all()
        ],
        'language_code': translation.get_language(),
        'language_bidi': translation.get_language_bidi(),
    }
