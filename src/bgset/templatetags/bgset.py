from django import template
from django.conf import settings
from django.utils.safestring import mark_safe

from easy_thumbnails.files import get_thumbnailer
from easy_thumbnails.exceptions import EasyThumbnailsError

import webcolors

register = template.Library()


@register.filter
def bgset(background):
    t = get_thumbnailer(background)

    html = ['data-sizes="auto"']
    srcset = []

    for name, config in settings.THUMBNAIL_ALIASES[''].items():
        try:
            t[name].url
        except EasyThumbnailsError:
            continue

        srcset.append(
            '%s %sw' % (t[name].url, config['size'][0])
        )

    html.append('data-bgset="%s"' % ', '.join(srcset))
    return mark_safe(' '.join(html))


@register.filter
def css_background_color(page):
    color = webcolors.hex_to_rgb(page.background_color)
    transparency = page.background_color_transparency / 100.0
    return 'rgba(%s, %s, %s, %s)' % (color + (transparency,))
